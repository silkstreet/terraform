variable "cloudflare_zone_id" {
  description = "The zone ID of the DNS zone to create the record in"
  type        = string
  default     = "3c930f60f74c6d5a9ea0d06c5ed7b96f"
}

resource "cloudflare_record" "caa" {
  zone_id = var.cloudflare_zone_id
  name    = "silkstreet.org"
  type    = "CAA"
  ttl     = 3600

  data {
    flags = 0
    tag   = "issue"
    value = "letsencrypt.org"
  }
}

# DNS for exchange online
resource "cloudflare_record" "autodiscover" {
  zone_id = var.cloudflare_zone_id
  name    = "autodiscover"
  value   = "autodiscover.outlook.com"
  type    = "CNAME"
  ttl     = 3600
}

resource "cloudflare_record" "mx" {
  zone_id  = var.cloudflare_zone_id
  name     = "silkstreet.org"
  value    = "silkstreet-org.mail.protection.outlook.com"
  type     = "MX"
  ttl      = 3600
  priority = 10
}

resource "cloudflare_record" "spf" {
  zone_id = var.cloudflare_zone_id
  name    = "silkstreet.org"
  value   = "v=spf1 include:spf.protection.outlook.com -all"
  type    = "TXT"
  ttl     = 3600
}
