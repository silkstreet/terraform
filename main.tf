terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.2.0"
    }
    civo = {
      source  = "civo/civo"
      version = "1.0.31"
    }
  }
}
