resource "civo_ssh_key" "oarj" {
  name       = "oarj"
  public_key = file("./files/id_ed25519_sk_rk.pub")
}

resource "civo_network" "cluster_network" {
  label  = "cluster_network"
  region = "fra1"
}

resource "civo_firewall" "cluster_firewall" {
  name                 = "cluster_firewall"
  network_id           = civo_network.cluster_network.id
  region               = "fra1"
  create_default_rules = false

  ingress_rule {
    label      = "kubernetes-api-server"
    protocol   = "tcp"
    port_range = "6443"
    cidr       = ["158.38.84.132/32"]
    action     = "allow"
  }

  egress_rule {
    label      = "all-tcp"
    protocol   = "tcp"
    port_range = "1-65535"
    cidr       = ["0.0.0.0/0"]
    action     = "allow"
  }
  egress_rule {
    label      = "all-udp"
    protocol   = "udp"
    port_range = "1-65535"
    cidr       = ["0.0.0.0/0"]
    action     = "allow"
  }
  depends_on = [civo_network.cluster_network]
}

resource "civo_kubernetes_cluster" "prd_cluster" {
  region       = "fra1"
  name         = "prd-cluster"
  applications = "argo-cd,metrics-server,Traefik-v2-nodeport"
  network_id   = civo_network.cluster_network.id
  firewall_id  = civo_firewall.cluster_firewall.id
  pools {
    size       = "g4s.kube.medium"
    node_count = 3
  }
  depends_on = [
    civo_network.cluster_network,
    civo_firewall.cluster_firewall
  ]
}
